﻿'use strict';
var app = angular.module('app',[]);
app.service('HttpService', HttpService);
app.service('PagerService', PagerService);
   

//Services for post/get a rest service
function HttpService($http) {
    var service = {};
    service.executeRequest = executeRequest;
    return service;

    //Executes an http call
    function executeRequest(url, method, data, contentType, token, success, error, progressArea) {
        var auth = null;
        method = method || 'GET';
        data = data || null;

        if (token) $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;

        if (contentType) $http.defaults.headers.common['Content-Type'] = contentType;

        if (progressArea) {
            waitProcess(progressArea);
        }

        return $http({
            method: method,
            url: url,
            data: data
            }).then(function (response) {
                $('#waitdiv').remove();
                success(response);
            }, function (response) {
                $('#waitdiv').remove();
                error(response);
            }
        );
    }

    //Show loading pane
    function waitProcess(target) {
        var div = document.createElement("DIV");
        div.id = 'waitdiv';
        div.style.position = 'fixed';
        div.style.display = 'block';
        div.style.width = '100%';
        div.style.height = '100%';
        div.style.backgroundColor = '#fff';
        div.style.zIndex = '100';
        div.style.top = 0;
        div.style.left = 0;
        div.style.opacity = 0.7;
        div.style.textAlign = 'center';
        div.style.backgroundImage = "url('/images/progress.gif')";
        div.style.backgroundRepeat = 'no-repeat';
        div.style.backgroundSize = '150px auto';
        div.style.backgroundPosition = 'center';
        $(target).append(div);
    }
}

function PagerService() {
    var service = {};
    service.getPager = getPager;

    return service;

    function getPager(totalItems, currentPage, pageSize) {
        // default to first page
        currentPage = currentPage || 1;

        // default page size is 10
        pageSize = pageSize || 10;

        // calculate total pages
        var totalPages = Math.ceil(totalItems / pageSize);

        var startPage, endPage;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        // calculate start and end item indexes
        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        var pages = createRangeArray(startPage, endPage + 1);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }

    //Creates an array with elements from start to end
    function createRangeArray(start, end) {
        var ret = [];
        for (var i = start; i < end; i++) {
            ret.push(i);
        }

        return ret;
    }
}
(function() {
    app.controller('reviewController',
        [
            '$scope', 'HttpService', '$timeout','PagerService',
            function ($scope, HttpService, $timeout, PagerService) {
                var vm = this;
                vm.pager = {};
                vm.setPage = setPage;
                var endPoint = 'http://v2.api.vvs.isnot.live/';
                $scope.averageScore = 0;
                $scope.listReviews = listReviews;
                $scope.pending = [];
                $scope.order = '1';
                $scope.rate = [false, false, false, false, false];
                $scope.selectedRate = 0;
                $scope.user = null;
                $scope.msg = '';
                $scope.reviews = [];

                listReviews('.list-area');

                function setPage(page) {
                    if (page < 1 || page > vm.pager.totalPages) {
                        return;
                    }

                    vm.pager = PagerService.getPager($scope.reviews.length, page);
                    vm.items = $scope.reviews.slice(vm.pager.startIndex, vm.pager.endIndex + 1);
                }

                //List the lastest 1000 reviews
                function listReviews(progressArea) {
                    var url = endPoint + '/api/log/listlog?order=' + $scope.order;

                    HttpService.executeRequest(url,
                        'GET',
                        null,
                        'application/json',
                        null,
                        successCallBack,
                        errorCallBack,
                        progressArea);

                    function successCallBack(response) {
                        if (response.data.Success) {
                            $scope.reviews = response.data.Data;
                            $scope.averageScore = parseInt(response.data.Message);
                            vm.setPage(Math.ceil($scope.reviews.length / 10));

                            $timeout(function() {
                                    $('.list-area').scrollTop($('.list-area')[0].scrollHeight);
                                },
                            100);
                        } else {
                            alert(response.data.Message);
                        }
                    }

                    function errorCallBack(response) {
                        alert('Fail to contact the service');
                    }
                }

                //Posts a review
                $scope.saveReview = function() {
                    var url = endPoint + '/api/log/savelog';
                    var data = {};
                    data.guid = guid();
                    data.name = $scope.user;
                    data.msg = $scope.msg;
                    data.score = $scope.selectedRate;
                    data.order = $scope.order;
                    data.pending = true;

                    addPending(data);

                    HttpService.executeRequest(url,
                        'POST',
                        data,
                        'application/json',
                        null,
                        successCallBack,
                        errorCallBack);

                    function successCallBack(response) {
                        if (response.data.Success) {
                            $scope.reviews = response.data.Data;
                            removePending(response.data.Data);
                            insertPendings($scope.reviews);
                            resetValues();
                            vm.setPage(Math.ceil($scope.reviews.length / 10));

                            $timeout(function() {
                                    $('.list-area').scrollTop($('.list-area')[0].scrollHeight);
                                },
                                300);
                        } else {
                            alert(response.data.Message);
                        }
                    }

                    function errorCallBack(response) {
                        alert('Fail to contact the service');
                    }
                }

                //Add a review in the pending list (reviews in post process)
                function addPending(data) {
                    $scope.pending.push(data);
                    $scope.reviews.push(data);
                    $timeout(function() {
                            $('.list-area').scrollTop($('.list-area')[0].scrollHeight);
                        },
                    300);
                }

                //Insert a pending review in the current review list
                function insertPendings(data) {
                    if ($scope.pending.length == 0) return;

                    for (var i = 0; i < $scope.pending.length; i++) {
                        data.push($scope.pending[i]);
                    }
                }

                //Removes a pending review from the list, after successfully posted
                function removePending(data) {
                    if ($scope.pending.length == 0) return;

                    for (var i = 0; i < data.length; i++) {
                        for (var x = 0; x < $scope.pending.length; x++) {
                            if ($scope.pending[x].guid == data[i].guid) {
                                $scope.pending.splice(x, 1);
                                return;
                            }
                        }
                    }
                }

                //check the rating stars on mouse-over event
                $scope.checkStars = function(rate) {
                    if (rate < $scope.selectedRate) return;

                    for (var i = 0; i < $scope.rate.length; i++) {
                        $scope.rate[i] = (i < rate);
                    }
                }

                //Uncheck de rating star in mouse-leave event
                $scope.unchekStars = function(rate) {
                    if ($scope.selectedRate > 0) {
                        for (var i = $scope.selectedRate; i < $scope.rate.length; i++) {
                            $scope.rate[i] = false;
                        }
                        return;
                    }

                    for (var i = 0; i < $scope.rate.length; i++) {
                        $scope.rate[i] = false;
                    }
                }

                //Select a rate (1-5)
                $scope.selectStar = function(rate) {
                    $scope.selectedRate = rate;
                    $scope.checkStars(rate);
                }

                //Reset all checks and selections
                function resetValues() {
                    $scope.rate = [false, false, false, false, false];
                    $scope.selectedRate = 0;
                    $scope.msg = '';
                }

                //Generates an GUID for identifying a review post
                function guid() {
                    function s4() {
                        return Math.floor((1 + Math.random()) * 0x10000)
                            .toString(16)
                            .substring(1);
                    }

                    return s4() +s4() +'-' +s4() +'-' +s4() +'-' +s4() +'-' +s4() +s4() +s4();
                }
            }
        ]);
})();