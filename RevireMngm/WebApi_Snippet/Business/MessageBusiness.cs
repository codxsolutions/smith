﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace VVSServices.Business
{
    public class MsgBusiness
    {
        /// <summary>
        /// Retrives app messages from web.config
        public static string AppMsg(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}