﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace VVSServices.Business
{
    public class ModelBusiness
    {
        //Transforms a model obj into an entity obj, and vice versa.
        public static T2 Transform<T1, T2>(T1 source, T2 target)
        {
            var objTarget = Activator.CreateInstance(typeof(T2));
            Type sourceType = source.GetType();
            List<PropertyInfo> sourceProps = new List<PropertyInfo>(sourceType.GetProperties());

            Type targetType = target.GetType();
            List<PropertyInfo> targetProps = new List<PropertyInfo>(targetType.GetProperties());

            foreach (PropertyInfo prop1 in targetProps)
            {
                foreach (PropertyInfo prop2 in sourceProps)
                {
                    if(prop1.Name.ToUpper() == prop2.Name.ToUpper())
                        {
                        PropertyInfo propertyInfo2 = objTarget.GetType().GetProperty(prop2.Name);
                        if (propertyInfo2 != null)
                        {
                            var propValue1 = prop2.GetValue(source, null);
                            propertyInfo2.SetValue(objTarget, Convert.ChangeType(propValue1, propertyInfo2.PropertyType), null);
                            break;
                        }                        
                    }
                }
            }

            return (T2)objTarget;
        }

        //Transforms a collection of model objs into a collection of entity objs, and vice versa.
        public static List<T1> TransformCollection<T1>(dynamic objOrigem, T1 objDestino)
        {
            List<T1> ret = new List<T1>();

            foreach (var item in objOrigem)
            {
                T1 obj = Transform(item, objDestino);
                ret.Add(obj);
            }

            return ret;
        }
    }
}