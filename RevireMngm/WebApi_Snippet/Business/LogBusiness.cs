﻿using VVSServices.Models.HttpRequest;
using VVSServices.Models.HttpResponse;
using VVSServices.Models;
using System;
using System.Linq;
using System.Collections.Generic;

namespace VVSServices.Business
{
    public class LogBusiness
    {
        public static OperationResponse<List<LogResponse>> ListLog(short order = 1)
        {
            try
            {
                using(var db = new vvsEntities())
                {
                    List<log> logs;

                    if (order == 1)
                    {
                        logs = db.log.Select(x => x).OrderByDescending(x => x.creation_date).Take(1000)
                               .OrderBy(x => x.creation_date).ToList();
                    }
                    else
                    {
                        logs = db.log.Select(x => x).OrderByDescending(x => x.creation_date).Take(1000)
                               .OrderBy(x => x.name).ThenBy(x=> x.creation_date).ToList();
                    }

                    var average = new
                    {
                        value = Math.Round(logs.Average(x => x.score), MidpointRounding.AwayFromZero)
                    };

                    var ret = ModelBusiness.TransformCollection(logs, new LogResponse());
                    return new OperationResponse<List<LogResponse>>(true, average.value.ToString(), ret);
                }

            }catch(Exception ex)
            {
                return new OperationResponse<List<LogResponse>>(false, ex.Message, null);
            }
        }

        public static OperationResponse<List<LogResponse>> SaveLog(LogRequest req)
        {
            try
            {
                using (var db = new vvsEntities())
                {
                    var newLog = new log()
                    {
                        creation_date = DateTime.Now,
                        msg = req.msg,
                        guid = req.guid,
                        name = req.name,
                        score = req.score
                    };

                    db.log.Add(newLog);
                    db.SaveChanges();

                    var ret = ListLog(req.order);
                    return ret;
                }
            }
            catch (Exception ex)
            {
                return new OperationResponse<List<LogResponse>>(false, ex.Message, null);
            }
        }
    }
}