﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VVSServices.Business;
using VVSServices.Models.HttpRequest;
using VVSServices.Models.HttpResponse;

namespace VVSServices.Controllers
{
    [RoutePrefix("api/log")]
    public class LogController : ApiController
    {
        [HttpGet]
        [AllowAnonymous]
        [Route("ListLog")]
        public OperationResponse<List<LogResponse>> ListLog(short order)
        {
            return LogBusiness.ListLog(order);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("SaveLog")]
        public OperationResponse<List<LogResponse>> SaveLog(LogRequest req)
        {
            return LogBusiness.SaveLog(req);
        }
    }
}
