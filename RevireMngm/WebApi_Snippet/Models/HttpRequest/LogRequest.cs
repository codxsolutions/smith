﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VVSServices.Models.HttpRequest
{
    public class LogRequest
    {
        public short order { get; set; }
        public string guid { get; set; }
        public DateTime creation_date { get; set; }
        public string name { get; set; }
        public string msg { get; set; }
        public short score { get; set; }
    }
}